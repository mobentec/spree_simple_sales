module Spree
  HomeController.class_eval do
	
	#rescue_from ActiveRecord::RecordNotFound, :with => :render_404
    helper 'spree/taxons'
	helper 'spree/products'

    respond_to :html
		
	def sale
      @products = Product.joins(:variants_including_master).where('spree_variants.sale_price is not null').uniq
    end
	
	def soaps
      @products = Spree::Product.in_taxons(1)
	 #@products = Spree::Product.joins(:taxons).where(Taxon.table_name => { :id => [3,4] })
	  @barsoap_products = Spree::Product.joins(:taxons).where(Taxon.table_name => { :id => [3] })
	  @Erica1 = "assets/store/soap.jpg"
	  @Erica2 = "Bar Soaps"
	  @taxonomies = Spree::Taxonomy.includes(root: :children)
    end
	
	def lipcare
      @products = Spree::Product.in_taxons(6)
	 #@products = Spree::Product.joins(:taxons).where(Taxon.table_name => { :id => [3,4] })
      @taxonomies = Spree::Taxonomy.includes(root: :children)
	  @Erica1 = "<img src=""assets/store/soap.jpg"" alt=""Gardener's scrub soap on the soaps page.""><h3>LIP CARE</h3>"


    end
	
	def bodycare
      @products = Spree::Product.in_taxons(7)
	  #@products = Spree::Product.joins(:taxons).where(Taxon.table_name => { :id => [3,4] })
      @taxonomies = Spree::Taxonomy.includes(root: :children)
	  #render('index')
	  @Erica1 = "<img src=""assets/store/soap.jpg"" alt=""Gardener's scrub soap on the soaps page.""><h3>HOME AND PET</h3>"
    end	

	def homepet
      @products = Spree::Product.in_taxons(7)
	  #@products = Spree::Product.joins(:taxons).where(Taxon.table_name => { :id => [3,4] })
      @taxonomies = Spree::Taxonomy.includes(root: :children)
	  #render('index')
	  @Erica1 = "<img src=""assets/store/soap.jpg"" alt=""Gardener's scrub soap on the soaps page.""><h3>HOME AND PET</h3>"
    end	

	def aboutus
	      @taxonomies = Spree::Taxonomy.includes(root: :children)

    end
	
	def upsale
		if product.has_related_products?('accessories')
 		 @upsalename = "true"
		end
	end
	
  end
 end
