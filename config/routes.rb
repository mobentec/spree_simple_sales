Spree::Core::Engine.routes.draw do
  # Add your extension routes here
  get "/sale" => "home#sale"
  get "/soaps" => "home#soaps"
  get "/lip-care" => "home#lipcare"
  get "/bodycare" => "home#bodycare"
  get "/homepet" => "home#homepet"
  get "/aboutus" => "home#aboutus"
end
